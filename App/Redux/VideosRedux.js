import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  videosRequest: [],
  likeRequest: ['index', 'currentState'],
  videosSuccess: ['videos'],
  videosFailure: ['error']
})

export const VideosTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  videos: null,
  fetching: null,
  error: null
})

/* ------------- Selectors ------------- */

export const GithubSelectors = {
  selectAvatar: state => state.github.avatar
}

/* ------------- Reducers ------------- */

export const request = (state) => {
  return state.merge({ fetching: true })
}

export const success = (state, action) => {
  const { videos } = action
  if (!state.videos) {
    return state.merge({ fetching: false, error: null, videos })
  } else {
    const previousVideos = _.cloneDeep(Immutable.asMutable(state.videos))
    _.forEach(videos, (v) => previousVideos.push(v))
    return state.merge({ fetching: false, error: null, videos: previousVideos })
  }
}

export const failure = (state, action) => {
  const { error } = action
  return state.merge({ fetching: false, error, videos: null })
}

export const likeRequest = (state, action)  => {
  const { index, currentState } = action
  const videos = _.cloneDeep(Immutable.asMutable(state.videos))
  videos[index].isLiked = !currentState
  return state.merge({ fetching: false, error: null, videos: videos })
}



/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VIDEOS_REQUEST]: request,
  [Types.LIKE_REQUEST]: likeRequest,
  [Types.VIDEOS_SUCCESS]: success,
  [Types.VIDEOS_FAILURE]: failure
})
