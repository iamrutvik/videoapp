import React, {Component} from 'react';
import {View, Dimensions, Image, Text, Slider, TouchableOpacity, Platform} from 'react-native';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation';
import {Metrics} from '../Themes'
import { Container, Button} from 'native-base'
import Colors from '../Themes/Colors'
import FAIcon from 'react-native-vector-icons/Feather'
import styles from './Styles/VideoScreenStyles'

function formatTime(second) {
  let h = 0, i = 0, s = parseInt(second);
  if (s > 60) {
    i = parseInt(s / 60);
    s = parseInt(s % 60);
  }
  let zero = function (v) {
    return (v >> 0) < 10 ? "0" + v : v;
  };
  return [zero(h), zero(i), zero(s)].join(":");
}

export default class VideoPlayScreen extends Component {


  constructor(props) {
    super(props);
    this.state = {
      videoUrl: this.props.navigation.getParam('url'),
      videoName: this.props.navigation.getParam('title'),
      videoWidth: Metrics.screenWidth,
      videoHeight: Metrics.screenWidth * 9/16,
      showVideoCover: false,
      showVideoControl: true,
      isPlaying: true,
      currentTime: 0,
      duration: 0,
      isFullScreen: true,
      playFromBeginning: false,
    };
  }
  componentDidMount () {
    Platform.OS=== 'ios' ?
      this.setState({isFullScreen: true}, () => {
      Orientation.lockToLandscape()
    }) : undefined
  }
  render() {
    return (
      <Container>
          <View style={styles.container} onLayout={this._onLayout}>
            <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', backgroundColor:'#000000' }}>
              <Video
                ref={(ref) => this.videoPlayer = ref}
                source={{uri: this.state.videoUrl}}
                rate={1.0}
                volume={1.0}
                muted={false}
                paused={!this.state.isPlaying}
                resizeMode={'contain'}
                playWhenInactive={false}
                playInBackground={false}
                ignoreSilentSwitch={'ignore'}
                progressUpdateInterval={250.0}
                onLoadStart={this._onLoadStart}
                onLoad={this._onLoaded}
                onProgress={this._onProgressChanged}
                onEnd={this._onPlayEnd}
                onError={this._onPlayError}
                onBuffer={this._onBuffering}
                style={{width: this.state.videoWidth, height: this.state.videoHeight}}
              />
              {
                this.state.showVideoCover ?
                  <Image
                    style={{
                      position:'absolute',
                      top: 0,
                      left: 0,
                      width: this.state.videoWidth,
                      height: this.state.videoHeight
                    }}
                    resizeMode={'cover'}
                    source={{uri: this.state.videoCover}}
                  /> : null
              }

              {
                this.state.showVideoControl ?
                  <View style={[styles.control, {width: this.state.videoWidth}]}>
                    <Button transparent onPress={() => { Orientation.lockToPortrait();this.props.navigation.goBack()}}>
                      <FAIcon name='arrow-left' color={Colors.snow} size={Metrics.icons.medium}/>
                    </Button>
                    <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlPlayPress() }}>
                      <Image
                        style={styles.playControl}
                        source={this.state.isPlaying ? require('../../assets/image/icon_control_pause.png') : require('../../assets/image/icon_control_play.png')}
                      />
                    </TouchableOpacity>
                    <Text style={styles.time}>{formatTime(this.state.currentTime)}</Text>
                    <Slider
                      style={{flex: 1}}
                      maximumTrackTintColor={'#999999'}
                      minimumTrackTintColor={'#00c06d'}
                      thumbImage={require('../../assets/image/icon_control_slider.png')}
                      value={this.state.currentTime}
                      minimumValue={0}
                      maximumValue={this.state.duration}
                      onValueChange={(currentTime) => { this.onSliderValueChanged(currentTime) }}
                    />
                    <Text style={styles.time}>{formatTime(this.state.duration)}</Text>
                    <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlShrinkPress() }}>
                      <Image
                        style={styles.shrinkControl}
                        source={this.state.isFullScreen ? require('../../assets/image/icon_control_shrink_screen.png') : require('../../assets/image/icon_control_full_screen.png')}
                      />
                    </TouchableOpacity>

                  </View> : null
              }
            </View>
          </View>
      </Container>

    )
  }


  _onLoadStart = () => {
    console.log('Load start');
  };

  _onBuffering = () => {
    console.log('_onBuffering')
  };

  _onLoaded = (data) => {
    console.log('_onLoaded');
    this.setState({
      duration: data.duration,
    });
  };

  _onProgressChanged = (data) => {
    console.log('_onProgressChanged');
    if (this.state.isPlaying) {
      this.setState({
        currentTime: data.currentTime,
      })
    }
  };

  _onPlayEnd = () => {
    console.log('_onPlayEnd');
    this.setState({
      currentTime: 0,
      isPlaying: false,
      playFromBeginning: true
    });
  };

  _onPlayError = () => {
    console.log('_onPlayError');
  };

  hideControl() {
    if (this.state.showVideoControl) {
      this.setState({
        showVideoControl: false,
      })
    } else {
      this.setState(
        {
          showVideoControl: true,
        },
        () => {
          setTimeout(
            () => {
              this.setState({
                showVideoControl: false
              })
            }, 5000
          )
        }
      )
    }
  }

  onPressPlayButton() {
    let isPlay = !this.state.isPlaying;
    this.setState({
      isPlaying: isPlay,
      showVideoCover: false
    });
    if (this.state.playFromBeginning) {
      this.videoPlayer.seek(0);
      this.setState({
        playFromBeginning: false,
      })
    }
  }
  onControlPlayPress() {
    this.onPressPlayButton();
  }

  onControlShrinkPress() {
    if (this.state.isFullScreen) {
      Orientation.lockToPortrait();
    } else {
      Orientation.lockToLandscape();
    }
  }

  onSliderValueChanged(currentTime) {
    this.videoPlayer.seek(currentTime);
    if (this.state.isPlaying) {
      this.setState({
        currentTime: currentTime
      })
    } else {
      this.setState({
        currentTime: currentTime,
        isPlaying: true,
        showVideoCover: false
      })
    }
  }

  _onLayout = (event) => {
    let {width, height} = event.nativeEvent.layout;
    let isLandscape = (width > height);
    if (isLandscape){
      this.setState({
        videoWidth: width,
        videoHeight: height,
        isFullScreen: true,
      })
    } else {
      this.setState({
        videoWidth: width,
        videoHeight: width * 9/16,
        isFullScreen: false,
      })
    }
    Orientation.unlockAllOrientations();
  };

  playVideo() {
    this.setState({
      isPlaying: true,
      showVideoCover: false
    })
  }

  pauseVideo() {
    this.setState({
      isPlaying: false,
    })
  }
}

