import React, { Component } from 'react'
import { ImageBackground, FlatList, View, Modal, TouchableOpacity, Platform } from 'react-native'
import { Images, Colors, Metrics } from '../Themes'
import { connect } from 'react-redux'
import VideosActions from '../Redux/VideosRedux'
import {
  Container,
  Header,
  Title,
  Content,
  Toast,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Text,
  Spinner,
  Card,
  CardItem
} from 'native-base';
// Styles
import styles from './Styles/LaunchScreenStyles'
import Icon from 'react-native-vector-icons/Feather'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Share from 'react-native-share';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
type LaunchScreenProps = {
  dispatch: () => any,
  fetching: boolean,
  attemptFetch: () => void,
  attemptLike: () => void,
};

class LaunchScreen extends Component {
  props: LaunchScreenProps;

  state: {
    loading: boolean,
    videos: Object
  };

  constructor (props: LaunchScreenProps) {
    super(props)

    this.state = {
      loading: false,
      videos: null
    }

    this.endThreshold = 0.7
    this.whoosh = null
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (!nextProps.fetching && prevState.loading) {
      if (nextProps.error) {
        Toast.show({
          text: nextProps.error,
          buttonText: 'Okay',
          type: 'danger',
          duration: 3000
        })
        return {
          loading: false
        }
      } else {
        return {
          loading: false,
          videos: nextProps.videos
        }
      }
    }
    return prevState
  }

  componentDidMount () {
    this.getVideos()
    Sound.setCategory('Playback');
    this.whoosh = new Sound('http://www.noiseaddicts.com/samples_1w72b820/3724.mp3', null, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
    });
  }
  getVideos = () => {
    this.setState({ loading: true }, () => {
      this.props.attemptFetch();
    })
  }
  handleLike = (index, currentState) => {
    this.setState({ loading:true }, () => {
      this.props.attemptLike(index, currentState)
      this.whoosh.play()
    })
  }
  handleShare = (url) => {
    Share.open({url: url})
      .then((res) => { console.log(res) })
      .catch((err) => { err && console.log(err); });
  }
  handlePlay = (url, title) => {
    this.props.navigation.navigate('VideoScreen', {url : url, title: title})
  }
  renderItem = ({item, index}) => {
    return (
      <Card style={{marginLeft: Platform.OS === 'ios' ? 0 : 20}}>
        <CardItem cardBody>
          <TouchableOpacity onPress={() => this.handlePlay(item.video_url, item.title.toUpperCase())}>
            <ImageBackground source={{uri: item.thumbnail_url}} style={styles.thumb}>
              <View style={styles.overlay}>
                <Icon name='play' color={Colors.bloodOrange} size={Metrics.icons.xl} />
              </View>
            </ImageBackground>
          </TouchableOpacity>
        </CardItem>
        <CardItem>
          <Left>
            <Button title='Like' onPress={() => this.handleLike(index, item.isLiked)} transparent>
            {item.isLiked ? <FAIcon name='heart' color={Colors.bloodOrange} size={Metrics.icons.medium}/>
              : <Icon name='heart' color={Colors.bloodOrange} size={Metrics.icons.medium}/>}
            </Button>
          </Left>
          <Body style={styles.titleContainer}>
            <Text>{item.title.toUpperCase()}</Text>
          </Body>
          <Right>
            <Button title='Share' onPress={() => this.handleShare(item.video_url)} transparent>
              <Icon name='share' color={Colors.bloodOrange} size={Metrics.icons.medium}/>
            </Button>
          </Right>
        </CardItem>
      </Card>
    )
  }
  renderFlatList = () => {
    if(this.state.videos){
      return (
        <FlatList
          data={this.state.videos}
          renderItem={this.renderItem}
          style={styles.list}
          keyExtractor={(item, key) => key.toString()}
          refreshing={this.state.loading}
          onRefresh={this.getVideos}
          onEndReachedThreshold={this.endThreshold}
          onEndReached={({distanceFromEnd}) => this.getVideos()}
        />
      )
    } else {
      return (
        <Spinner color={Colors.bloodOrange} />
      )
    }
  }
  render () {
    return (
      <Container>
        <Header style={styles.header}>
          <Left style={{flex:1}}/>
          <Body style={{flex:1}}>
            <Title style={styles.headerTitle}>Videos</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        {this.renderFlatList()}
      </Container>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    fetching: state.videos.fetching,
    videos: state.videos.videos,
    error: state.videos.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptFetch: () => dispatch(VideosActions.videosRequest()),
    attemptLike: (index, currentState) => dispatch(VideosActions.likeRequest(index, currentState))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)

