import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  list: {
    padding: 10
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  thumb: {
    height: 200,
    width: null,
    flex: 1,
    justifyContent:'center',
    alignItems: 'center'
  },
  playIcon: {
    backgroundColor: 'rgba(45,45,45,0.7)',
    padding: 10,
  },
  overlay: {
    backgroundColor:'rgba(45,45,45,0.5)',
    height: 200,
    width: Metrics.screenWidth - 24,
    justifyContent:'center',
    alignItems: 'center'
  },
  backgroundVideo: {
    position: 'absolute',
    top: 70,
    bottom: 0,
    left: 0,
    right: 0,
    width: Metrics.screenWidth,
    height: null
  },
  closeModal: {
    position: 'absolute',
    top: 50,
    right: 10,
  },
  maxbutton: {
    position: 'absolute',
    top: 50,
    right: 40,
  }
})
