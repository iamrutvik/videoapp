import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import ReduxPersist from '../Config/ReduxPersist'
import {SafeAreaView} from 'react-navigation'
import Colors from '../Themes/Colors'
import { Root } from 'native-base'

// Styles
import styles from './Styles/RootContainerStyles'

class RootContainer extends Component {
  componentDidMount () {
    console.disableYellowBox = true
  }

  render () {
    return (
      <SafeAreaView style={styles.applicationView} forceInset={{ top: 'never', bottom: 'never' }}>
        <StatusBar barStyle='light-content' backgroundColor={Colors.snow} />
        <Root>
          <ReduxNavigation />
        </Root>
      </SafeAreaView>
    )
  }
}


export default connect(null, null)(RootContainer)
