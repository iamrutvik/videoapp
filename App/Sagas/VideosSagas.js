import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
import VideosActions from '../Redux/VideosRedux'
import _ from 'lodash'
export function * fetch (api) {
  // make the call to the api
  const videos = [
    {
      "title":"video small",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg",
      "video_url":"https://www.w3schools.com/htmL/mov_bbb.mp4",
      "isLiked": false
    },{
      "title":"video medium",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ElephantsDream.jpg",
      "video_url":"http://techslides.com/demos/sample-videos/small.mp4",
      "isLiked": false
    },{
      "title":"video medium & long",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerMeltdowns.jpg",
      "video_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      "isLiked": false
    },{
      "title":"video big",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerEscapes.jpg",
      "video_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      "isLiked": false
    },{
      "title":"video small",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg",
      "video_url":"https://www.w3schools.com/htmL/mov_bbb.mp4",
      "isLiked": false
    },{
      "title":"video medium",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ElephantsDream.jpg",
      "video_url":"http://techslides.com/demos/sample-videos/small.mp4",
      "isLiked": false
    },{
      "title":"video medium & long",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerMeltdowns.jpg",
      "video_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      "isLiked": false
    },{
      "title":"video big",
      "thumbnail_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerEscapes.jpg",
      "video_url":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      "isLiked": false
    }]
  yield put(VideosActions.videosSuccess(videos))

  /*const response = yield call(api.getVideos)
  if (response.status === 200 && response.ok) {
    let videos = response.data.videos
    let newVideos = _.forEach(videos, (item) => {
      item.isLiked = false
    })
    yield put(VideosActions.videosSuccess(newVideos))
  } else {
    switch (response.problem) {
      case 'NETWORK_ERROR':
        yield put(VideosActions.videosFailure('Network Connection is not available'))
        break
      case 'CONNECTION_ERROR':
        yield put(VideosActions.videosFailure('Server is not available'))
        break
      default:
        yield put(VideosActions.videosFailure('Something went wrong'))
        break
    }
  }*/
}

